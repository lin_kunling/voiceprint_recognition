#!/data/anaconda3/bin/python
import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram
import keras
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dropout, Activation
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import confusion_matrix
import os
import pandas as pd
import librosa
import glob 





from keras.models import model_from_json

from keras.utils import np_utils



from keras.utils import np_utils
from sklearn.utils import shuffle


import matplotlib.pyplot as plt
import scipy.io.wavfile
import numpy as np
import sys



from keras.models import model_from_json


from sklearn.preprocessing import LabelEncoder





lb = LabelEncoder()
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("saved_models/Emotion_Voice_Detection_Model.h5")
print("Loaded model from disk")





data, sampling_rate = librosa.load(sys.argv[1])
#data, sampling_rate = librosa.load('output10.wav')

#plt.figure(figsize=(15, 5))
#librosa.display.waveplot(data, sr=sampling_rate)
X, sample_rate = librosa.load(sys.argv[1], res_type='kaiser_fast',duration=2.5,sr=22050*2,offset=0.5)
#X, sample_rate = librosa.load('output10.wav', res_type='kaiser_fast',duration=2.5,sr=22050*2,offset=0.5)
sample_rate = np.array(sample_rate)
mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13),axis=0)
featurelive = mfccs
livedf2 = featurelive
livedf2= pd.DataFrame(data=livedf2)
#print(livedf2)
print(livedf2[:10])
livedf2 = livedf2.stack().to_frame().T
twodim= np.expand_dims(livedf2, axis=2)
#livepreds = loaded_model.predict(twodim, 
#                         batch_size=32, 
#                         verbose=1)
#print(livepreds)
#livepreds1=livepreds.argmax(axis=1)
#print("ok")
#print(livepreds1)
#liveabc = livepreds1.astype(int).flatten()
#print(liveabc)
#livepredictions = (lb.inverse_transform((liveabc)))
#print(livepredictions)



preds = loaded_model.predict(twodim, 
                         batch_size=32, 
                         verbose=1)

print(preds)
preds1=preds.argmax(axis=1)

abc = preds1.astype(int).flatten()
print(abc)
if abc == 0 :
    print("female_angry")
elif abc == 1 :
    print("female_calm")
elif abc == 2 :
    print("female_fearful")
elif abc == 3 :
    print("female_happy")
elif abc == 4 :
    print("female_sad")
elif abc == 5 :
    print("male_angry")
elif abc == 6 :
    print("male_calm")
elif abc == 7 :
    print("male_fearful")
elif abc == 8 :
    print("male_happy")
elif abc == 9 :
    print("male_sad")
#predictions = (lb.inverse_transform((abc)))
#0 - female_angry 
#1 - female_calm 
#2 - female_fearful 
#3 - female_happy 
#4 - female_sad 
#5 - male_angry 
#6 - male_calm 
#7 - male_fearful 
#8 - male_happy 
#9 - male_sad 
#predictions = (lb.inverse_transform((abc)))
#0 - female_angry 
#1 - female_calm 
#2 - female_fearful 
#3 - female_happy 
#4 - female_sad 
#5 - male_angry 
#6 - male_calm 
#7 - male_fearful 
#8 - male_happy 
#9 - male_sad 

#preddf = pd.DataFrame({'predictedvalues': predictions})
#print(preddf)
