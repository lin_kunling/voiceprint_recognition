#!/usr/bin/python
#encoding=utf-8
import urllib2
import sys,time
import sys
sys.path.append("/home/lzj/shell/jieba-0.31/")
import jieba
import threading
import SocketServer
import jieba.posseg as pseg
import jieba.analyse
jieba.initialize()

#url = sys.argv[1]
#url2 = sys.argv[2]
#content = open(url,"rb")
#log_f = open(url2,"w")
#for line in content:
#t1 = time.time()
#	words = " ".join(jieba.cut(line))
#t2 = time.time()
#tm_cost = t2-t1
#	log_f.write(words.encode('utf-8'))

#print 'cost',tm_cost
#print 'speed' , len(content)/tm_cost, " bytes/second"

#!/usr/bin/env python

users = []

class MyTCPHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        username = None
        while True:
            self.data = self.rfile.readline().strip()
            cur_thread = threading.currentThread()
#            print "RECV from ", self.client_address[0]
            cmd = self.data
            if cmd == None or len(cmd) == 0:
                break;
            # business logic here
            try:
		a=cmd.split()
		i3=len(a) 
		if i3 == 3 :
			testp="pos"
			if a[2] == testp:
				result = pseg.cut(a[0])
				jn=0
				posres=[]
				for w in result:
					self.wfile.write(w.word.encode('utf-8'))
					self.wfile.write("/")
					self.wfile.write(w.flag)
					self.wfile.write(' ')
				self.wfile.write('\n')
				break;
			elif a[2] == "restart":
				reload(jieba)
				jieba.initialize()
				self.wfile.write('ok')
				self.wfile.write('\n')
				break;
			elif a[2] == "key":
				print "key" + a[0]
				an=jieba.analyse.extract_tags(a[0],9)
				ankey=" ".join(an)
				self.wfile.write(ankey.encode('utf-8'))
				break;
				
			else:
				words = " ".join(jieba.cut(a[0]))	
				self.wfile.write(words.encode('utf-8'))
				self.wfile.write('\n')
				break;
			
		else:
			url=a[0]
			url2=a[1]
			content = open(url,"rb")
			log_f = open(url2,"w")
			for line in content:
				words = " ".join(jieba.cut(line))
				log_f.write(words.encode('utf-8'))
			log_f.close
                	result = '1'
                	self.wfile.write(result)
                	self.wfile.write('\n')
			break;
            except:
                print 'error'
                break
        try:
            if username != None:
                users.remove(username)
        except:
            pass
        print username, ' closed.'

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "localhost",11222

    server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.setDaemon(True)
    server_thread.start()
    server.serve_forever()
