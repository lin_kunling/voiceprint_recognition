# -*-coding: utf-8 -*-

import json
import requests
import re

# SERVER = 'http://127.0.0.1'
SERVER = 'http://172.26.1.7'
USER = '047941'

def chat(string):
    url = '{}:3000/api/chat/{}/{}'.format(SERVER, USER, string)
    print('\t{}'.format(url))
    try:
        resp = requests.get(url, timeout=10)
    except Exception as e:
        return False, '\tHttp请求失败：{}'.format(str(e))
    if resp.status_code == 200:
        return True, resp.text
    return False, resp.status_code

def test_chat(feature, string, refer_result=None):
    print('test_{}...'.format(feature))
    chat_result, chat_resp = chat(string)
    if chat_result and chat_resp:
        print('\tresponse: {}'.format(chat_resp))
        if refer_result and refer_result != chat_resp.strip('\n'):
            return False
        return True
    print('\tresponse: False, status_code: {}'.format(chat_resp))
    return False

def test_memory(string1, string2, string3):
    print('test_memory...')
    string = '记住{}'.format(string1)
    chat_result, chat_resp = chat(string)
    if not chat_result or not re.search(r'记住', chat_resp):
        print('记忆失败, {}'.format(chat_resp))
        return False
    result = test_chat('记忆效果', string2, '<train>'+string3)
    if result:
        print('\t记忆成功')
        return True
    print('\t记忆失败')
    return False

def test_tiaojiao(string1, string2):
    print('test_tiaojiao...')
    string = '我说{}的时候你就说{}'.format(string1, string2)
    chat_result, chat_resp = chat(string)
    if not chat_result or not re.search(r'学会', chat_resp):
        print('\t调教失败, {}'.format(chat_resp))
        return False
    train_result = test_chat('调教效果', string1, '<train>'+string2)
    if train_result:
        print('\t调教成功')
        return True
    print('\t调教失败')
    return False

def test_upload():
    print('test_upload...')
    payload = {
        'uid': USER,
        'msg':
            """插座一\t1\t1\tna
打开\t1\t1\tst"""
    }
    url = '{}:3005/upload'.format(SERVER)
    try:
        resp = requests.post(url, data=json.dumps(payload), timeout=10)
    except Exception as e:
        print('\thttp请求失败, {}'.format(str(e)))
        return False
    if resp.status_code != 200 or resp.text != 'ok':
        print('\t上传失败, {}'.format(resp.status_code, resp.text))
        return False
    print('\t上传结果：{}'.format('ok'))
    upload_result = test_chat('配置上传', '<bname=HuaShu>打开插座一')
    if upload_result:
        print('\t配置上传成功')
        return True
    print('\t配置上传失败')
    return False


result = {}
result['hello'] = test_chat('hello', '你好啊')
result['nomsolo'] = test_chat('nomsolo', '以前我是不想生娃娃的', '计划生育观念不错嘛')
result['rule'] = test_chat('rule', '学个鸭子叫', '嘎，嘎嘎')
result['gushi'] = test_chat('gushi', '背首古诗来')
result['tiaojiao'] = test_tiaojiao('老大尽力了', '队友CBA')
result['memory'] = test_memory('你是键盘侠', '你是什么侠', '我是键盘侠')
result['upload'] = test_upload()
print('--------------------------------------------------------')
fail_result = [k for k, v in result.items() if v != True]
print('失败：{}'.format(fail_result))
fail_len = len(result) - len(fail_result)
print('通过率：{}/{}'.format(fail_len, len(result)))
