#!/usr/bin/python
#encoding=utf-8
import urllib2
import sys,time
import sys
sys.path.append("./")
import jieba
import threading
import SocketServer
jieba.initialize()

#url = sys.argv[1]
#url2 = sys.argv[2]
#content = open(url,"rb")
#log_f = open(url2,"w")
#for line in content:
#t1 = time.time()
#	words = " ".join(jieba.cut(line))
#t2 = time.time()
#tm_cost = t2-t1
#	log_f.write(words.encode('utf-8'))

#print 'cost',tm_cost
#print 'speed' , len(content)/tm_cost, " bytes/second"

#!/usr/bin/env python

users = []

class MyTCPHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        username = None
        while True:
            self.data = self.rfile.readline().strip()
            cur_thread = threading.currentThread()
#            print "RECV from ", self.client_address[0]
            cmd = self.data
            if cmd == None or len(cmd) == 0:
                break;
       	    if cmd == "restart":
		reload(jieba)
		jieba.initialize()
		self.wfile.write('ok')
		self.wfile.write('\n')
		break;
#            print cmd
            # business logic here
            try:
	#	line=cmd.encode('utf-8')
#		line=cmd
#		a=cmd.split()
#		url=a[0]
#		url2=a[1]
#		content = open(url,"rb")
#		log_f = open(url2,"w")
#		for line in content:
		words1 = " ".join(jieba.cut(cmd))
#		print words1
#		words = " ".join(jieba.cut(line))
#		log_f.write(words.encode('utf-8'))
#		log_f.close
       #         result = '1'
#		words='你好'
		words=words1.encode('utf-8')
                self.wfile.write(words)
                #self.wfile.write(words.decode('utf-8'))
                self.wfile.write('\n')
		break;
            except:
                print 'error'
                break
        try:
            if username != None:
                users.remove(username)
        except:
            pass
        print username, ' closed.'

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "localhost",11229

    server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.setDaemon(True)
    server_thread.start()
    server.serve_forever()
