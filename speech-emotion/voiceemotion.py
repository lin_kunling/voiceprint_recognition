#!/home/zb/anaconda3/bin/python
#encoding=utf-8
import sys
import datetime
import sys,time
import sys
#from LunarSolarConverter import LunarSolarConverter
#sys.path.append("/home/lzj/shell/jieba-0.31/")
#import jieba
import threading
import socketserver
#import jieba.posseg as pseg
#import jieba.analyse
#jieba.initialize()

import librosa
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram
import keras
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input, Flatten, Dropout, Activation
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from sklearn.metrics import confusion_matrix
import os
import pandas as pd
import librosa
import glob 
from keras.models import model_from_json
from keras.utils import np_utils
from keras.utils import np_utils
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import scipy.io.wavfile
import numpy as np
import sys
from keras.models import model_from_json
from sklearn.preprocessing import LabelEncoder



#        lb = LabelEncoder()
#        json_file = open('model.json', 'r')
#        loaded_model_json = json_file.read()
#        json_file.close()
#        loaded_model = model_from_json(loaded_model_json)
#        # load weights into new model
#        loaded_model.load_weights("saved_models/Emotion_Voice_Detection_Model.h5")
#        print("Loaded model from disk")




common_used_numerals={u'零':0,u'一':1,u'二':2,u'三':3,u'四':4,u'五':5,u'六':6,u'七':7,u'八':8,u'九':9,u'十':10,u'百':100,u'千':1000,u'万':10000,u'亿':100000000,u'两':2}
c=u"三千二百二十二"
def cn2digits(uchars_cn):
#    print("running")
#    print(uchars_cn)
    s=uchars_cn
    if not s :
        return 0
    for i in [u'亿',u'万',u'千',u'百',u'十']:
        if i in s:
            ps=s.split(i)
            lp=cn2digits(ps[0])
            if lp==0:
                lp=1
            rp=cn2digits(ps[1])
            #print i,s,lp,rp
            return lp*common_used_numerals.get(i, 0)+rp
    return common_used_numerals.get(s[-1], 0)
#print cn2digits(c)
#print cn2digits(cc)
# -*- coding: utf--8 -*-  




  
def alb2ch(numin):
    resyy=[]
    chiNum=['零','一','二','三','四','五','六','七','八','九']  
    chiSerie=['零','十','百','千']  
#num=raw_input("9999")  
#num=raw_input("请输入您要转换的介于0~9999的数字：")  
    zeroFlag=0  
    flag=0  
    num=int(numin)
    for i in range(4):  
        level=int(num/pow(10,3-i))
        if level==0 and flag==1:  
            zeroFlag=1  
        elif level!=0:  
            flag=1  
            num=num%pow(10,3-i)  
            if zeroFlag==1:  
                resyy.append(chiNum[0])
            resyy.append(chiNum[level])
            if i!=3:  
                resyy.append(chiSerie[3-i])
            zeroFlag=0  
    rskey="".join(resyy)
    return rskey
#if zeroFlag==1:
#                        sys.stdout.write(chiNum[0])
#                sys.stdout.write( chiNum[level])
#                if i!=3:
#                        sys.stdout.write( chiSerie[3-i])
#                zeroFlag=0

class Lunar(object):
    #******************************************************************************
    # 下面为阴历计算所需的数据,为节省存储空间,所以采用下面比较变态的存储方法.
    #******************************************************************************
    #数组g_lunar_month_day存入阴历1901年到2050年每年中的月天数信息，
   #阴历每月只能是29或30天，一年用12（或13）个二进制位表示，对应位为1表30天，否则为29天
   g_lunar_month_day = [
       0x4ae0, 0xa570, 0x5268, 0xd260, 0xd950, 0x6aa8, 0x56a0, 0x9ad0, 0x4ae8, 0x4ae0,   #1910
       0xa4d8, 0xa4d0, 0xd250, 0xd548, 0xb550, 0x56a0, 0x96d0, 0x95b0, 0x49b8, 0x49b0,   #1920
       0xa4b0, 0xb258, 0x6a50, 0x6d40, 0xada8, 0x2b60, 0x9570, 0x4978, 0x4970, 0x64b0,   #1930
       0xd4a0, 0xea50, 0x6d48, 0x5ad0, 0x2b60, 0x9370, 0x92e0, 0xc968, 0xc950, 0xd4a0,   #1940
       0xda50, 0xb550, 0x56a0, 0xaad8, 0x25d0, 0x92d0, 0xc958, 0xa950, 0xb4a8, 0x6ca0,   #1950
       0xb550, 0x55a8, 0x4da0, 0xa5b0, 0x52b8, 0x52b0, 0xa950, 0xe950, 0x6aa0, 0xad50,   #1960
       0xab50, 0x4b60, 0xa570, 0xa570, 0x5260, 0xe930, 0xd950, 0x5aa8, 0x56a0, 0x96d0,   #1970
       0x4ae8, 0x4ad0, 0xa4d0, 0xd268, 0xd250, 0xd528, 0xb540, 0xb6a0, 0x96d0, 0x95b0,   #1980
       0x49b0, 0xa4b8, 0xa4b0, 0xb258, 0x6a50, 0x6d40, 0xada0, 0xab60, 0x9370, 0x4978,   #1990
       0x4970, 0x64b0, 0x6a50, 0xea50, 0x6b28, 0x5ac0, 0xab60, 0x9368, 0x92e0, 0xc960,   #2000
       0xd4a8, 0xd4a0, 0xda50, 0x5aa8, 0x56a0, 0xaad8, 0x25d0, 0x92d0, 0xc958, 0xa950,   #2010
       0xb4a0, 0xb550, 0xb550, 0x55a8, 0x4ba0, 0xa5b0, 0x52b8, 0x52b0, 0xa930, 0x74a8,   #2020
       0x6aa0, 0xad50, 0x4da8, 0x4b60, 0x9570, 0xa4e0, 0xd260, 0xe930, 0xd530, 0x5aa0,   #2030
       0x6b50, 0x96d0, 0x4ae8, 0x4ad0, 0xa4d0, 0xd258, 0xd250, 0xd520, 0xdaa0, 0xb5a0,   #2040
       0x56d0, 0x4ad8, 0x49b0, 0xa4b8, 0xa4b0, 0xaa50, 0xb528, 0x6d20, 0xada0, 0x55b0,   #2050
   ]

   #数组gLanarMonth存放阴历1901年到2050年闰月的月份，如没有则为0，每字节存两年
   g_lunar_month = [
       0x00, 0x50, 0x04, 0x00, 0x20,   #1910
       0x60, 0x05, 0x00, 0x20, 0x70,   #1920
       0x05, 0x00, 0x40, 0x02, 0x06,   #1930
       0x00, 0x50, 0x03, 0x07, 0x00,   #1940
       0x60, 0x04, 0x00, 0x20, 0x70,   #1950
       0x05, 0x00, 0x30, 0x80, 0x06,   #1960
       0x00, 0x40, 0x03, 0x07, 0x00,   #1970
       0x50, 0x04, 0x08, 0x00, 0x60,   #1980
       0x04, 0x0a, 0x00, 0x60, 0x05,   #1990
       0x00, 0x30, 0x80, 0x05, 0x00,   #2000
       0x40, 0x02, 0x07, 0x00, 0x50,   #2010
       0x04, 0x09, 0x00, 0x60, 0x04,   #2020
       0x00, 0x20, 0x60, 0x05, 0x00,   #2030
       0x30, 0xb0, 0x06, 0x00, 0x50,   #2040
       0x02, 0x07, 0x00, 0x50, 0x03    #2050
   ]

   START_YEAR = 1901
   
   # 天干
   gan = '甲乙丙丁戊己庚辛壬癸'
   # 地支
   zhi = '子丑寅卯辰巳午未申酉戌亥'
   # 生肖
   xiao = '鼠牛虎兔龙蛇马羊猴鸡狗猪'
   # 月份
   lm = '正二三四五六七八九十冬腊'
   # 日份
   ld = '初一初二初三初四初五初六初七初八初九初十十一十二十三十四十五十六十七十八十九二十廿一廿二廿三廿四廿五廿六廿七廿八廿九三十'
   # 节气
   jie = '小寒大寒立春雨水惊蛰春分清明谷雨立夏小满芒种夏至小暑大暑立秋处暑白露秋分寒露霜降立冬小雪大雪冬至'
   
   def __init__(self, dt = None):
       '''初始化：参数为datetime.datetime类实例，默认当前时间'''
       self.localtime = dt if dt else datetime.datetime.today()
       
   def sx_year(self): # 返回生肖年
       ct = self.localtime #取当前时间
       
       year = self.ln_year() - 3 - 1 # 农历年份减3 （说明：补减1）
       year = year % 12 # 模12，得到地支数
       return self.xiao[year]

   def gz_year(self): # 返回干支纪年
       ct = self.localtime #取当前时间
       year = self.ln_year() - 3 - 1 # 农历年份减3 （说明：补减1）
       G = year % 10 # 模10，得到天干数
       Z = year % 12 # 模12，得到地支数
       return self.gan[G] + self.zhi[Z]

   def gz_month(self): # 返回干支纪月（未实现）
       pass

   def gz_day(self): # 返回干支纪日
       ct = self.localtime #取当前时间
       C = ct.year // 100 #取世纪数，减一
       y = ct.year % 100 #取年份后两位（若为1月、2月则当前年份减一）
       y = y - 1 if ct.month == 1 or ct.month == 2 else y
       M = ct.month #取月份（若为1月、2月则分别按13、14来计算）
       M = M + 12 if ct.month == 1 or ct.month == 2 else M
       d = ct.day #取日数
       i = 0 if ct.month % 2 == 1 else 6 #取i （奇数月i=0，偶数月i=6）
       
       #下面两个是网上的公式
       # http://baike.baidu.com/link?url=MbTKmhrTHTOAz735gi37tEtwd29zqE9GJ92cZQZd0X8uFO5XgmyMKQru6aetzcGadqekzKd3nZHVS99rewya6q
       # 计算干（说明：补减1）
       G = 4 * C + C // 4 + 5 * y + y // 4 + 3 * (M + 1) // 5 + d - 3 - 1
       G = G % 10
       # 计算支（说明：补减1）
       Z = 8 * C + C // 4 + 5 * y + y // 4 + 3 * (M + 1) // 5 + d + 7 + i - 1
       Z = Z % 12

      #返回 干支纪日
       return self.gan[G] + self.zhi[Z]

   def gz_hour(self): # 返回干支纪时（时辰）
      ct = self.localtime #取当前时间
      #计算支
      Z = round((ct.hour/2) + 0.1) % 12 # 之所以加0.1是因为round的bug!!
      
      #返回 干支纪时（时辰）
      return self.zhi[Z]

   def ln_year(self): # 返回农历年
      year, _, _ = self.ln_date()
      return year

   def ln_month(self): # 返回农历月
      _, month, _ = self.ln_date()
      return month

   def ln_day(self): # 返回农历日
      _, _, day = self.ln_date()
      return day

   def ln_date(self): # 返回农历日期整数元组（年、月、日）（查表法）
      delta_days = self._date_diff()

      #阳历1901年2月19日为阴历1901年正月初一
      #阳历1901年1月1日到2月19日共有49天
      if (delta_days < 49):
          year = self.START_YEAR - 1
          if (delta_days <19):
            month = 11; 
            day = 11 + delta_days
          else:
              month = 12;
              day = delta_days - 18
          return (year, month, day)

      #下面从阴历1901年正月初一算起
      delta_days -= 49
      year, month, day = self.START_YEAR, 1, 1
      #计算年
      tmp = self._lunar_year_days(year)
      while delta_days >= tmp:
          delta_days -= tmp
          year += 1
          tmp = self._lunar_year_days(year)

      #计算月
      (foo, tmp) = self._lunar_month_days(year, month)
      while delta_days >= tmp:
          delta_days -= tmp
          if (month == self._get_leap_month(year)):
              (tmp, foo) = self._lunar_month_days(year, month)
              if (delta_days < tmp):
                  return (0, 0, 0)
              delta_days -= tmp
          month += 1
          (foo, tmp) = self._lunar_month_days(year, month)

      #计算日
      day += delta_days
      return (year, month, day)
  
   def ln_date_str(self):# 返回农历日期字符串，形如：农历正月初九
      _, month, day = self.ln_date()
      return '农历{}月{}'.format(self.lm[month-1], self.ld[(day-1)*2:day*2])
  
   def ln_jie(self): # 返回农历节气
      ct = self.localtime #取当前时间
      year = ct.year
      for i in range(24):
          #因为两个都是浮点数，不能用相等表示
          delta = self._julian_day() - self._julian_day_of_ln_jie(year, i)
          if -.5 <= delta <= .5:
              return self.jie[i*2:(i+1)*2]
      return ''
  
  #显示日历
   def calendar(self):
      pass

  #######################################################
  #            下面皆为私有函数
  #######################################################
  
   def _date_diff(self):
      '''返回基于1901/01/01日差数'''
      return (self.localtime - datetime.datetime(1901, 1, 1)).days

   def _get_leap_month(self, lunar_year):
      flag = self.g_lunar_month[(lunar_year - self.START_YEAR) // 2]
      if (lunar_year - self.START_YEAR) % 2:
          return flag & 0x0f
      else:
          return flag >> 4

   def _lunar_month_days(self, lunar_year, lunar_month):
      if (lunar_year < self.START_YEAR):
          return 30

      high, low = 0, 29
      iBit = 16 - lunar_month;

      if (lunar_month > self._get_leap_month(lunar_year) and self._get_leap_month(lunar_year)):
          iBit -= 1

      if (self.g_lunar_month_day[lunar_year - self.START_YEAR] & (1 << iBit)):
          low += 1
         
      if (lunar_month == self._get_leap_month(lunar_year)):
          if (self.g_lunar_month_day[lunar_year - self.START_YEAR] & (1 << (iBit -1))):
               high = 30
          else:
               high = 29

      return (high, low)

   def _lunar_year_days(self, year):
      days = 0
      for i in range(1, 13):
          (high, low) = self._lunar_month_days(year, i)
          days += high
          days += low
      return days
  
  # 返回指定公历日期的儒略日（http://blog.csdn.net/orbit/article/details/9210413）
   def _julian_day(self):
      ct = self.localtime #取当前时间
      year = ct.year
      month = ct.month
      day = ct.day

      if month <= 2:
          month += 12
          year -= 1

      B = year / 100
      B = 2 - B + year / 400

      dd = day + 0.5000115740 #本日12:00后才是儒略日的开始(过一秒钟)*/
      return int(365.25 * (year + 4716) + 0.01) + int(30.60001 * (month + 1)) + dd + B - 1524.5    
  
  # 返回指定年份的节气的儒略日数（http://blog.csdn.net/orbit/article/details/9210413）
   def _julian_day_of_ln_jie(self, year, st):
      s_stAccInfo =[
           0.00, 1272494.40, 2548020.60, 3830143.80, 5120226.60, 6420865.80,
           7732018.80, 9055272.60, 10388958.00, 11733065.40, 13084292.40, 14441592.00,
           15800560.80, 17159347.20, 18513766.20, 19862002.20, 21201005.40, 22529659.80,
           23846845.20, 25152606.00, 26447687.40, 27733451.40, 29011921.20, 30285477.60]

      #已知1900年小寒时刻为1月6日02:05:00
      base1900_SlightColdJD = 2415025.5868055555
      
      if (st < 0) or (st > 24):
          return 0.0
   
      stJd = 365.24219878 * (year - 1900) + s_stAccInfo[st] / 86400.0
   
      return base1900_SlightColdJD + stJd


      
      
# 测试
def test(ct=None):
  ln = Lunar(ct)    
  reso='公历 {}  北京时间 {}'.format(ln.localtime.date(), ln.localtime.time()) + '{} 【{}】 {}年 {}日 {}时'.format(ln.ln_date_str(), ln.gz_year(), ln.sx_year(), ln.gz_day(), ln.gz_hour()) +'节气：{}'.format(ln.ln_jie())
  print('公历 {}  北京时间 {}'.format(ln.localtime.date(), ln.localtime.time()))
  print('{} 【{}】 {}年 {}日 {}时'.format(ln.ln_date_str(), ln.gz_year(), ln.sx_year(), ln.gz_day(), ln.gz_hour()))
  print('节气：{}'.format(ln.ln_jie()))
  return reso


  

#url = sys.argv[1]
#url2 = sys.argv[2]
#content = open(url,"rb")
#log_f = open(url2,"w")
#for line in content:
#t1 = time.time()
#	words = " ".join(jieba.cut(line))
#t2 = time.time()
#tm_cost = t2-t1
#	log_f.write(words.encode('utf-8'))

#print 'cost',tm_cost
#print 'speed' , len(content)/tm_cost, " bytes/second"


users = []

class MyTCPHandler(socketserver.StreamRequestHandler):
    def handle(self):
        username = None


        while True:
            self.data = self.rfile.readline().strip()
            cur_thread = threading.currentThread()
#            print "RECV from ", self.client_address[0]
            cmd = self.data
            #print(cmd)
            if cmd == None or len(cmd) == 0:
                break;
            try:
                a=cmd.split()
                i3=len(a) 
       #         print(i3)
      #          print(a[1])
     #           print(a[2])
                sn=int(a[0])
                if i3 == 3 :
    #                    print("eqqqqqqqqqqq")
                        if sn == 1:
    #                            print("yeseqqqqqqqqqqqqqq")
                                inarg=str(a[1]).strip('b\'').split('_')
                                inarg2=str(a[2]).strip('b\'').split('_')
                                lnsn=len(inarg2)
     #                           print(a[1])
      #                          print(inarg[0])
       #                         print('1')
        #                        print(inarg[1])
         #                       print('2')
          #                      print(inarg[2])
                                if lnsn ==3 :
                                     ct = datetime.datetime(int(inarg[0]),int(inarg[1]),int(inarg[2]),int(inarg2[0]),int(inarg2[1]),int(inarg2[2]))
                                else:
                                     ct = datetime.datetime(int(inarg[0]),int(inarg[1]),int(inarg[2]),13,0,15)

                                print("here")
                                ress=test(ct)
                                print("----------")
                                print(ress)
                                print("=============")
                                self.wfile.write(ress.encode("utf-8"))
#@words.encode('utf-8')
                                self.wfile.write('\n')
                                break;
                        elif sn == 2:
                                inarg=str(a[1]).strip('b\'').split('_')
                                #print("9999999999999")
#                                ct = datetime.datetime(int(inarg[0]),int(inarg[1]),int(inarg[2]),int(inarg2[0]),int(inarg2[1]),int(inarg2[2])
                                #print(allstr)
                                solar = LunarSolarConverter.Lunar(int(inarg[0]), int(inarg[1]), int(inarg[2]),False)
                                solar2 = converter.LunarToSolar(solar)
                                allstr=str(solar2.solarYear)+ "_"+str(solar2.solarMonth)+"_"+str(solar2.solarDay)
                                self.wfile.write(allstr.encode("utf-8"))
#                                self.wfile.write(str(solar2.solarYear)+ "_"+str(solar2.solarMonth)+"_"+str(solar2.solarDay))
                                self.wfile.write('\n')
                                break; 	
                        elif sn == 3:
                                inarg=a[1]
#                                print(inarg)
#                                print("111111111")
                                inarg2=inarg.decode("utf-8")
#                                print("222222")
#                                print(inarg2)
                                resss=str(cn2digits(inarg2))
                                print("3333")
                                print(resss)
                                self.wfile.write(resss.encode("utf-8"))
#                                self.wfile.write(inarg)
                                self.wfile.write('\n')
                                break;
                        elif sn == 4:
                                inarg=a[1]
                                print("444444444444444")
                                print(inarg)
                                inarg2=str(inarg).strip('b\'')
                                print("inarg2")
                                print(inarg2)
                                resss=alb2ch(inarg2)
                                print("5555555555555")
                                print(resss)
                                self.wfile.write(resss.encode("utf-8"))
                                self.wfile.write('\n')
                                break;
                        elif sn == 5:
                                fp=a[1]
                                print(fp)
                                resa=testvoice(fp)
                                print(resa)
                                self.wfile.write(resa.encode("utf-8"))
                                self.wfile.write('\n')

#print cn2digits(cc.decode("utf-8"))
                        break;
                break;
            except:
                print('error')
                break
        try:
            if username != None:
                users.remove(username)
        except:
            pass

def testvoice(filepath):
    data, sampling_rate = librosa.load(filepath)
    print("load file")
    print(filepath)
#data, sampling_rate = librosa.load('output10.wav')

#plt.figure(figsize=(15, 5))
#librosa.display.waveplot(data, sr=sampling_rate)
    X, sample_rate = librosa.load(filepath, res_type='kaiser_fast',duration=2.5,sr=22050*2,offset=0.5)
#X, sample_rate = librosa.load('output10.wav', res_type='kaiser_fast',duration=2.5,sr=22050*2,offset=0.5)
    sample_rate = np.array(sample_rate)
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13),axis=0)
    featurelive = mfccs
    livedf2 = featurelive
    livedf2= pd.DataFrame(data=livedf2)
#print(livedf2)
    print(livedf2[:10])
    livedf2 = livedf2.stack().to_frame().T
    twodim= np.expand_dims(livedf2, axis=2)
    print("twodim")
    print(twodim)
#livepreds = loaded_model.predict(twodim, 
#                         batch_size=32, 
#                         verbose=1)
#print(livepreds)
#livepreds1=livepreds.argmax(axis=1)
#print("ok")
#print(livepreds1)
#liveabc = livepreds1.astype(int).flatten()
#print(liveabc)
#livepredictions = (lb.inverse_transform((liveabc)))
#print(livepredictions)
    preds = loaded_model.predict(twodim, batch_size=32, verbose=1)
    print("preds")
    print(preds)
    preds1=preds.argmax(axis=1)
    print("preds1")
    print(preds1)
    ress=" "
    abc = preds1.astype(int).flatten()
    print(abc)
    print("abc===")
    if abc == 0 :
        ress="female_angry"
        print("female_angry")
    elif abc == 1 :
        ress="female_calm"
        print("female_calm")
    elif abc == 2 :
        ress="female_fearful"
        print("female_fearful")
    elif abc == 3 :
        ress="female_happy"
        print("female_happy")
    elif abc == 4 :
        ress="female_sad"
        print("female_sad")
    elif abc == 5 :
        ress="male_angry"
        print("male_angry")
    elif abc == 6 :
        ress="male_calm"
        print("male_calm")
    elif abc == 7 :
        ress="male_fearful"
        print("male_fearful")
    elif abc == 8 :
        ress="male_happy"
        print("male_happy")
    elif abc == 9 :
        ress="male_sad"
        print("male_sad")
    return ress

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "localhost",15123

    lb = LabelEncoder()
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("saved_models/Emotion_Voice_Detection_Model.h5")
    print("Loaded model from disk")
    testvoice("female_1.wav");
    server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)
    server.allow_reuse_address = True
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.setDaemon(True)
    server_thread.start()
    server.serve_forever()








