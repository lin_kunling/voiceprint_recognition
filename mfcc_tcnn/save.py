class Save(keras.callbacks.Callback):
    def __init__(self):
        self.min_loss = 1.0
        tmodelsavepath = mysavemodelpath
        tmymodelfiles = list()
        tmymodelfiles = getpathfiles(tmodelsavepath, tmymodelfiles)
        if (len(tmymodelfiles) > 0):
            mname = tmymodelfiles[0]  # kears_model_05_acc=0.7319999933242798.h5
            i1 = mname.find("_loss=")
            i2 = mname.find(".h5")
            scroestr = mname[i1 + 1:i2]
            self.max_acc = float(scroestr)

    def on_epoch_begin(self, epoch, logs=None):
        pass

    def on_epoch_end(self, epoch, logs=None):
        self.val_loss = logs["val_loss"]
        self.acc = logs["val_acc"]
        if epoch != 0:
            if self.val_loss < self.min_loss:
                ename = str(epoch)
                if (epoch > 0):
                    ename = "0" + ename
                sname = str(samplecount) + "_"
                del_file(mysavemodelpath)
                model.save(mysavemodelpath + "/kears_model_" + sname + ename + "_acc=" + str(self.acc)+ "_loss=" + str(self.val_loss) + ".h5")
                self.min_loss = self.val_loss
                print("kears_model_ Save")
