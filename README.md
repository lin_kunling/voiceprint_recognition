# 声纹识别

## VGG_bak
通过VGG解决说话人识别代码。

## bear_user
再机器人ros环境下训练及测试。

## mfcc_tcnn
基于mfcc+tcnn模型解决说话人识别问题。

## speaker_recognition
第一个版本的mfcc+gmm模型，用于说话人识别。

## speaker_new_gmm
第二个版本的mfcc+gmm模型，用于说话人识别，比第一个gmm运行速度快很多且准确率较高。

## speech-emotion
语音情绪及性别识别。

## VAD
基于音频做滤波等预处理。

## webrtvad
基于webrtvad模块对音频做端点检测。
