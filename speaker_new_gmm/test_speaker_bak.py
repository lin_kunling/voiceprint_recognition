#test_gender.py
import os
import _pickle as cPickle
import numpy as np
from scipy.io.wavfile import read
from speakerfeatures import extract_features
import warnings
warnings.filterwarnings("ignore")
import time


#path to training data
source   = "development_set/"

modelpath = "speaker_models/"

test_file = "test.txt"

file_paths = open(test_file ,'r')
print(file_paths)

gmm_files = [os.path.join(modelpath,fname) for fname in 
              os.listdir(modelpath) if fname.endswith('.gmm')]

#Load the Gaussian gender Models
models    = [cPickle.load(open(fname,'rb')) for fname in gmm_files]
speakers   = [fname.split("\\")[-1].split(".gmm")[0] for fname 
              in gmm_files]
#print(speakers)

#softmax
def softmax(x):
    return np.exp(x)/np.sum(np.exp(x),axis=0)

# Read the test directory and get the list of test audio files 
for path in file_paths:
    path = path.strip()   
    print(path)
    sr,audio = read(source + path)
    vector   = extract_features(audio,sr)
    #print(vector)
    log_likelihood = np.zeros(len(models))
    #print(log_likelihood)
    for i in range(len(models)):
        gmm    = models[i]         #checking with each model one by one
        scores = np.array(gmm.score(vector))
        log_likelihood[i] = scores.sum()
    winner = np.argmax(log_likelihood)
    print(max(softmax(log_likelihood)))
    print("\tdetected as - ", speakers[winner])
    time.sleep(1.0)


