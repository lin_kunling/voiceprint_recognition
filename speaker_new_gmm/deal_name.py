# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 16:30:23 2019

@author: Administrator
"""

import os,sys
path=sys.argv[1]
for category in os.listdir(path):
    people_path = os.path.join(path, category)
    if os.path.isdir(people_path):
        for audio in os.listdir(people_path):
            os.rename(os.path.join(people_path,audio), os.path.join(people_path ,audio.replace(' ', '_').replace('(','_').replace(')','')))
    elif os.path.isfile(people_path):
        os.rename(people_path, os.path.join(path, category.replace(' ', '_').replace('(','_').replace(')','')))
        