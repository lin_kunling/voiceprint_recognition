import sys,os
def mp3_wav(path):
    for category in os.listdir(path):
        catdir = os.path.join(path,category)
        if os.path.isdir(catdir):
            for mp3file in os.listdir(path + category +'/'):
                os.path.splitext(mp3file)[1] ==  'mp3'
                filename = os.path.splitext(mp3file)[0]
                os.system('ffmpeg -i ' + path + category + '/' + filename + '.mp3 -f wav ' + path + category +'/' + filename + '.wav')
                os.system('ffmpeg -i ' + path + category + '/' + filename + '.wav -ar 16000 ' + path + category + '/' + filename + '_16k.wav')
                os.system('ffmpeg -i ' + path + category + '/' + filename + '_16k.wav -ac 1 ' + path + category + '/' + filename + '_16k_mono.wav')
                os.system('rm '+ path + category + '/' + filename + '.wav')
                os.system('rm ' + path + category + '/' + filename + '_16k.wav')
mp3_wav(sys.argv[1])
