# 声纹识别（说话人识别）
有两种方法
## 一、若想输入为wav
### 第一种方法是mfcc + tcnn
步骤：1、要进入mfcc_tcnn_train_wav.py修改训练集，训练集目前只支持wav格式然后运行
      2、python3 mfcc_tcnn_test_wav.py进行测试，要开启ros的audio

### 第二种方法是gmm
训练代码：python3 gmm_train_wav.py -t enroll -i "gmm_wav/*" -m model_gmm/wav_1.out
要进入gmm_test_wav.py修改模型地址
测试代码：python3 gmm_test_wav.py,要开启ros的audio

## 二、若想输入为txt
### 第一种方法是mfcc + tcnn
步骤：1、要进入mfcc_tcnn_train.py修改训练集，训练集目前只支持wav格式然后运行
      2、python3 mfcc_tcnn_test.py进行测试，要开启ros的audio


### 第二种方法是gmm
训练代码：python3 gmm_train.py -t enroll -i "gmm_wav/*" -m model_gmm/wav_1.out
要进入gmm_test.py修改模型地址
测试代码：python3 gmm_test.py,要开启ros的audio

# 新的声纹识别（gmm，说话人识别与说话人辨认结合）########
统一会从转换成wav输入。
### 训练代码
把音频文件夹放进audio_wav/audio_wav_2中。
然后运行python3 gmm2_train_wav.py audio_path model_path
audio_path是音频保存地址，model_path是gmm2模型保存地址

### 测试音频代码
运行gmm2_test.wav.py

# VGG16说话人辨认
### 1、首先运行VGG-Backup/src/train_predict_sore.py训练出每个说话人的分数。
分数结果保存在VGG-Backup/result/score_list90.txt
如果要修改训练音频路径，要修改VGG-Backup/src/train_predict_sore.py代码中的25行的data_path

### 2、然后运行VGG-Backup/src/predict_test.py预测未知音频。
结果保存在在VGG-Backup/result/predict_score_list.txt
同样要修改VGG-Backup/src/predict_test.py里面的26,27行的训练音频和测试音频的地址。

此方法最高准确率可以达到97%，可是时效性比较差，因为VGG16是一个音频一个音频去对比，不像gmm会保存模型统一训练。

