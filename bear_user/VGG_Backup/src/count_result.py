import numpy as np
import pandas as pd
predict_score_txt = pd.read_csv('../result/no_sur/predict_score_list.txt',sep = ',', header = None)
#result_txt = pd.read_csv('../result/score_list_min.txt',sep = ',', header = None)
result_txt = pd.read_csv('../result/no_sur/score_list90.txt',sep = ',', header = None)

predict_score_frame = predict_score_txt.rename( columns={0:'no', 1:'test_wav', 2:'test_speaker',
                                                         3:'train_speaker' , 4:'pre_max_score'})
result_frame = result_txt.rename(columns={0:'no', 1:'train_speaker', 2:'max_score'})

#print(predict_score_frame.head())
#print(result_frame.head())
frame = pd.merge(predict_score_frame, result_frame, on='train_speaker', how='inner')

frame = frame.drop(['no_x', 'no_y'] , 1)
#print(frame)

#对于一个wav，选择最大的分数，然后大于此人的75%分数，则认为是该人。
t = 0
test_wav_unique = frame['test_wav'].unique()
for testwav in test_wav_unique:
    frame_wav = frame[frame['test_wav']== testwav]
    #print(frame_wav)
    index_row = frame_wav[frame_wav['pre_max_score'].isin([max(frame_wav['pre_max_score'])])]
    print(index_row['test_wav'].iloc[0,],index_row['test_speaker'].iloc[0,],index_row['train_speaker'].iloc[0,])
    print(index_row['pre_max_score'].iloc[0,],index_row['max_score'].iloc[0,])
    if index_row['pre_max_score'].iloc[0,] > index_row['max_score'].iloc[0,] :
        if index_row['test_speaker'].iloc[0,] == index_row['train_speaker'].iloc[0,] :
            t = t+1
            print(t)
    elif index_row['pre_max_score'].iloc[0,] < index_row['max_score'].iloc[0,] :
        if index_row['test_speaker'].iloc[0,] != index_row['train_speaker'].iloc[0,] :
            t = t+1
            print(t)


    #print(index_row)
#print(t,len(test_wav_unique))




# pre_label = []
# i= 0
# for i in range(len(frame)):
#     #if frame['pre_max_score'][i] > frame['max_score'][i] :
#     if frame['pre_max_score'][i] > 0.85:
#         pre_label.append('true')
#     else :pre_label.append('false')
# #print(pre_label)
#
# fact_label = []
# i= 0
# for i in range(len(frame)):
#     if frame['train_speaker'][i] == frame['test_speaker'][i] :
#         fact_label.append('true')
#     else :fact_label.append('false')
#
# pre_label_frame = pd.DataFrame(pre_label,columns = ['pre_label'])
# fact_label_frame = pd.DataFrame(fact_label,columns = ['fact_label'])
# count_true = 0
# i= 0
# for i in range(len(pre_label_frame)):
#     if pre_label[i] == fact_label[i] : count_true = count_true + 1
#
# end = pd.concat([frame, pre_label_frame, fact_label_frame] ,axis = 1)
# #print(end)
#
# print('准确率为：',count_true/(len(pre_label_frame)))

#end.to_csv('../result/predict_result.txt', header = 0)