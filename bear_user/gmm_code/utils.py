from scipy.io import wavfile

def read_wav(fname):
    fs, signal = wavfile.read(fname)#读取.wav音频文件,第一项为音频的采样率，第二项为音频数据的numpy数组
    if len(signal.shape) != 1:
        print("convert stereo to mono")
        signal = signal[:,0]
    return fs, signal



