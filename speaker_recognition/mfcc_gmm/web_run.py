# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 11:50:00 2019

@author: Administrator
"""

import os,sys
import datetime


def webrtc_vad(senti_num,int_path,out_path,model_name):
    now_start = datetime.datetime.now()
    start_time = now_start.strftime("%Y-%m-%d %H:%M:%S")
    for category in os.listdir(int_path):
        people_path = int_path + '/' + category
        os.mkdir(out_path + '/' +category)
        for audio in os.listdir(people_path):
            int_audio_path = people_path + '/' +audio
            out_audio_path = '/zcha/voiceprint/web_audio/' + category +'/' +audio
            cmd = 'python3 webrtc_vad.py ' + str(senti_num) + ' ' + int_audio_path + ' ' +out_audio_path 
            os.system(cmd)
    run_sp = 'python3 speaker-recognition_top1.py -t enroll -i "'
    for people_name in  os.listdir(out_path):
        run_sp = run_sp + out_path + '/' + people_name +'/ '
    run_sp = run_sp + '*.wav " -m ' + model_name +'.out'
    os.system(run_sp)
    now_end =  datetime.datetime.now()
    end_time = now_end.strftime("%Y-%m-%d %H:%M:%S")
    print('start time:',start_time , '\nend time:',end_time  )

webrtc_vad(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])          
