#### 简书地址如下：https://www.jianshu.com/p/840e0f74d165

### linux安装


#### 代码如下：

```
yum -y install epel-release
yum -y install python-pip
yum -y install python-devel
pip install webrtcvad
```

#### 如果出现gcc报错：

```
yum install -y libffi-devel openssl-devel #安装其他依赖
yum -y install gcc#安装gcc
```

#### 再继续安装：

```
pip install webrtcvad
```

